package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.trounamentConst.TournamantConst;
import com.nespresso.sofa.recruitement.tournament.wepon.Wepon;

public class GreatSword extends Wepon {
	private Integer attackNumber;

	public GreatSword() {
		super.dmg = TournamantConst.DMG_GREAT_SWORD;
		this.attackNumber = 0;
	}

	public Integer getAttackNumber() {
		return attackNumber;
	}
	
	

}
