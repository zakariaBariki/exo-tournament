package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.trounamentConst.TournamantConst;

public class Buckler {
	
	private Integer numberOfBlockByAxe;

	public Buckler() {
		this.numberOfBlockByAxe = TournamantConst.BOCKLER_NUMB_BLOCK;
	}

	public Integer getNumberOfBlockByAxe() {
		return numberOfBlockByAxe;
	}
	
	public void decreaseNumbOfBlock(){
		this.numberOfBlockByAxe --;
	}
	

}
