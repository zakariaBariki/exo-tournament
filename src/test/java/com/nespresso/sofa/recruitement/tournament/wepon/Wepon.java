package com.nespresso.sofa.recruitement.tournament.wepon;

public class Wepon {
	
	protected Integer dmg;

	public Integer getDmg() {
		return dmg;
	}

	public void decreaseDmg(Integer decreaseValue){
		this.dmg -= decreaseValue;
	}
	public void doubleDmg(){
		this.dmg *= 2;
	}
	

}
