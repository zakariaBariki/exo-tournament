package com.nespresso.sofa.recruitement.tournament.trounamentConst;

public class TournamantConst {
	
	public static final Double HIT_SWORDMAN = new Double(100);
	public static final Double HIT_HIHGLANDER = new Double(150);
	public static final Double HIT_VIKING = new Double(120);
	public static final Integer DMG_HAND_AXE = 6;
	public static final Integer DMG_HAND_SWORD = 5;
	public static final Integer DMG_GREAT_SWORD = 12;
	public static final Integer BOCKLER_NUMB_BLOCK = 5;
	public static final Integer NUMB_REDUCED_DMG_ARMOR = 3;
	public static final Integer NUMB_REDUCED_ATTACK_ARMOR = 1;
	public static final Integer POISON_POWER = 20;
	public static final Double BERSE_VALUE = 150*0.3;
}
