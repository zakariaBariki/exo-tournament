package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.Armor;
import com.nespresso.sofa.recruitement.tournament.Buckler;
import com.nespresso.sofa.recruitement.tournament.GreatSword;
import com.nespresso.sofa.recruitement.tournament.trounamentConst.TournamantConst;
import com.nespresso.sofa.recruitement.tournament.wepon.HandAxe;
import com.nespresso.sofa.recruitement.tournament.wepon.Wepon;

public abstract class Fitgher {

	protected Double point;
	protected Wepon wepon;
	protected Buckler buckler;
	protected Armor armor;

	public Fitgher(Double point, Wepon wepon) {
		this.point = point;
		this.wepon = wepon;
	}

	public Fitgher(Double point, Wepon wepon, Buckler buckler) {
		this.point = point;
		this.wepon = wepon;
		this.buckler = buckler;
	}

	public void engage(Fitgher fitgher) {
		engageWithBuckler(fitgher);
		engageWithArmor(fitgher);
		fight(fitgher);

	}

	public void engageWithArmor(Fitgher fitgher) {
		if (checkIfHasAnArmor(fitgher)) {
			fitgher.wepon.decreaseDmg(TournamantConst.NUMB_REDUCED_ATTACK_ARMOR);
			this.wepon.decreaseDmg(TournamantConst.NUMB_REDUCED_DMG_ARMOR);
		}
		if (checkIfHasAnArmor(this)) {
			this.wepon.decreaseDmg(TournamantConst.NUMB_REDUCED_ATTACK_ARMOR);
			fitgher.wepon.decreaseDmg(TournamantConst.NUMB_REDUCED_DMG_ARMOR);
		}
	}

	private void engageWithBuckler(Fitgher fitgher) {
		if (checkBuckler(fitgher) && checkBuckler(this)) {
			Integer nuberOfEngage = 1;
			if (nuberOfEngage % 2 != 0) {
				fight(fitgher);
			} else {
				nuberOfEngage++;
			}
			if (checkIfWeponIsAnAxe(this.wepon)) {
				// this has an axe
				fitgher.buckler.decreaseNumbOfBlock();
			} else if (checkIfWeponIsAnAxe(fitgher.wepon)) {
				this.buckler.decreaseNumbOfBlock();
			}

		}
	}

	public void fight(Fitgher fitgher) {
		Integer greatSowrdCheck = 1;
		while (this.point > 0 && fitgher.point > 0) {
			if (checkHitPoint(fitgher.point, this.wepon.getDmg())) {
				if (fitgher.checkIfHasGs(fitgher.wepon)) {
					fightWithGreatSword(fitgher, greatSowrdCheck);
				}else{
					fitgher.decreaseHitPoint(this.wepon.getDmg());
				}
			}
			if (checkHitPoint(this.point, fitgher.wepon.getDmg())) {
				if (fitgher.checkIfHasGs(fitgher.wepon)) {
					fightWithGreatSword(this, greatSowrdCheck);
				}else{
					this.decreaseHitPoint(fitgher.wepon.getDmg());
				}

			}

		}
	}

	private void fightWithGreatSword(Fitgher fitgher, Integer greatSowrdCheck) {
		if (greatSowrdCheck == 1 || greatSowrdCheck % 3 != 0) {
			fitgher.decreaseHitPoint(this.wepon.getDmg());
		}
	}

	public Fitgher equip(String type) {
		if (type != null) {
			if (type.equals("buckler"))
				this.buckler = new Buckler();
			if (type.equals("armor"))
				this.armor = new Armor();
		}

		return this;
	}

	public Double hitPoints() {
		return this.point;
	}

	public static Boolean checkHitPoint(Double hitPointA, Integer hitPointB) {
		return hitPointA - hitPointB > 0;
	}

	public void decreaseHitPoint(Integer dmg) {
		this.point -= dmg;
	}

	public Boolean checkBuckler(Fitgher fitgher) {
		if (fitgher.buckler != null) {
			return true;
		} else {
			return false;
		}

	}

	public Boolean checkIfHasGs(Wepon wepon) {
		if (wepon instanceof GreatSword) {
			return true;
		} else {
			return false;
		}

	}

	public Boolean checkIfWeponIsAnAxe(Wepon wepon) {
		if (wepon instanceof HandAxe) {
			return true;
		} else {
			return false;
		}
	}

	public Boolean checkIfHasAnArmor(Fitgher fitgher) {
		if (fitgher.armor != null) {
			return true;
		} else {
			return false;
		}

	}

}
